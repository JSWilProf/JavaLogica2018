package logica.ex04.respostas;

import javax.swing.JOptionPane;

import logica.lib.Util;

public class Ex02 {
	public static void main(String[] args) {
		int a = Util.leInteiro("Informe o lado A");
		int b = Util.leInteiro("Informe o lado B");
		int c = Util.leInteiro("Informe o lado C");
		
		String triangulo;
		if(a == b && b == c) {
			triangulo = "Eqüilátero";
		} else if(a != b && a != c && b != c) {
			triangulo = "Escaleno";
		} else /* if(a == b || a == c || b == c) */ {
			triangulo = "Isósceles";
		}
		
		JOptionPane.showMessageDialog(null, triangulo);
	}
}
