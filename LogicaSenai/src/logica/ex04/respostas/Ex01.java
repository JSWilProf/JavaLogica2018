package logica.ex04.respostas;

import javax.swing.JOptionPane;

import logica.lib.Util;

public class Ex01 {
	public static void main(String[] args) {
		int num1 = Util.leInteiro("Informe a 1ª nota");
		int num2 = Util.leInteiro("Informe a 2ª nota");
		int num3 = Util.leInteiro("Informe a 3ª nota");
		int num4 = Util.leInteiro("Informe a 4ª nota");
		
		double media = (num1 + num2 + num3 + num4) / 4;
		
		String msg = "Reprovado";
		if(media >= 7) {
			msg = "Aprovado";
		}
		
		JOptionPane.showMessageDialog(null, msg);
	}
}
