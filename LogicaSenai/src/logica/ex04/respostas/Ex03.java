package logica.ex04.respostas;

import javax.swing.JOptionPane;

import logica.lib.Util;

public class Ex03 {
	public static void main(String[] args) {
		int a = Util.leInteiro("Informe o valor de A");
		int b = Util.leInteiro("Informe o valor de B");
		int c = Util.leInteiro("Informe o valor de C");

		double delta = Math.pow(b, 2) - 4 * a * c;
		if(delta >= 0) {
			int x1 = (int)(-b + Math.sqrt(delta))/ 2 * a;
			int x2 = (int)(-b - Math.sqrt(delta))/ 2 * a;
			
			JOptionPane.showMessageDialog(null,
					"x' = " + x1 + 
					"\nx\" = " + x2
					);
		} else {
			JOptionPane.showMessageDialog(null, "A raiz é um nº complexo");
		}
	}
}
