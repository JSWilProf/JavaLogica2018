package logica.ex05.exemplos;

import javax.swing.JOptionPane;

import logica.lib.Util;

public class ContadorFor {
	public static void main(String[] args) {
		int num = Util.leInteiro("Informe um numero");

		while (num > 0) {
			num *= 10;
			JOptionPane.showMessageDialog(null, num);
			
			num = Util.leInteiro("Informe um numero");
		}
	}
}
