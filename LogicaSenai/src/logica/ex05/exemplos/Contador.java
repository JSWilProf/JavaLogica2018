package logica.ex05.exemplos;

import javax.swing.JOptionPane;

import logica.lib.Util;

public class Contador {
	public static void main(String[] args) {
		int qtd = Util.leInteiro("Informe a quantidade");
		
		String msg = "Valores gerados\n\n";
		for (int i = 1; i <= qtd; i += 2) {
			int num = i * 10;
			msg += num + "\n";
		}
		JOptionPane.showMessageDialog(null, msg);
	}
}
