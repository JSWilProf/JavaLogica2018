package logica.ex05.respostas;

import static logica.lib.Util.leInteiro;

import javax.swing.JOptionPane;

public class Ex04x {
	public static void main(String[] args) {
		int qtd =  leInteiro("Informe a quantidade");
		
		if(qtd > 0) {
			int num = leInteiro("Informe um nº");
			int oMaior = num;
			
			
			for (int i = 1;;i++) {
			    if(num > oMaior) {
					oMaior = num;
				}	
			    
			    if(i < qtd) {
			    	num = leInteiro("Informe um nº");
			    } else {
			    	break;
			    }
			}
			
			JOptionPane.showMessageDialog(null,"O maior: " + oMaior);
		} else {
			JOptionPane.showMessageDialog(null, "Nenhum valor foi informado");
		}
	}
}
