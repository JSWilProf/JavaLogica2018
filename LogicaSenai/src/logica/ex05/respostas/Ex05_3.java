package logica.ex05.respostas;

import logica.lib.Util;

public class Ex05_3 {
	public static void main(String[] args) {
		int idade = Util.leInteiro("Informe a idade");
		int oVeio = idade;
		
		while(idade > 0) {
			if(idade > oVeio) {
				oVeio = idade;
			}
			
			idade = Util.leInteiro("Informe a idade");
		}
		
		Util.escreval("O ganhador foi o ilustrissimo com a idade de: ", oVeio);
	}
}
