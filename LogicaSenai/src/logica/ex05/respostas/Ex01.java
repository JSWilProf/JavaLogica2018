package logica.ex05.respostas;

import javax.swing.JOptionPane;

public class Ex01 {
	public static void main(String[] args) {
		String msg = "";
		for (int i = 2,k = 1; i <= 1000; i+=2,k++) {
			if(k >= 22) {
				msg += "\n";
				k = 1;
			}
			msg += String.format("%04d ", i);
		}
		JOptionPane.showMessageDialog(null, msg);
	}
}
