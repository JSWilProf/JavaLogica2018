package logica.ex05.respostas;

import javax.swing.JOptionPane;

public class Ex01b {
	public static void main(String[] args) {
		String msg = "";
		for (int i = 2; i <= 1000; i+=2) {
			msg += String.format("%04d ", i);
			if(i % 42 == 0) {
				msg += "\n";
			}
		}
		JOptionPane.showMessageDialog(null, msg);
	}
}
