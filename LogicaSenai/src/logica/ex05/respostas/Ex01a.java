package logica.ex05.respostas;

import javax.swing.JOptionPane;

public class Ex01a {
	public static void main(String[] args) {
		String msg = "";
		int k = 1;
		for (int i = 2; i <= 1000; i+=2) {
			if(k >= 22) {
				msg += "\n";
				k = 1;
			}
			msg += String.format("%04d ", i);
			k++;
		}
		JOptionPane.showMessageDialog(null, msg);
	}
}
