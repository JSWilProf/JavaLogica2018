package logica.ex01.exemplos;

import javax.swing.JOptionPane;

public class Calculo {
	public static void main(String[] args) {
		String temp = JOptionPane.showInputDialog("Informe o 1º nº");
		int num1 = Integer.parseInt(temp);
		
		temp = JOptionPane.showInputDialog("Informe o 2º nº");
		int num2 = Integer.parseInt(temp);
		
		int total = num1 * num2;
		
		JOptionPane.showMessageDialog(null, "O Total é: " + total);
	}
}
