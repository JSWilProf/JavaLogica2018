package logica.ex02;

import javax.swing.JOptionPane;

public class Exercicio2b {
	public static void main(String[] args) {
		String temp = JOptionPane.showInputDialog("Informe a 1ª nota");
		double total = Double.parseDouble(temp);

		temp = JOptionPane.showInputDialog("Informe a 2ª nota");
		total += Double.parseDouble(temp);

		temp = JOptionPane.showInputDialog("Informe a 3ª nota");
		total += Double.parseDouble(temp);

		temp = JOptionPane.showInputDialog("Informe a 4ª nota");
		total += Double.parseDouble(temp);
		
		total /= 4;
		
		String msg = String.format("A média é de: %,.2f" , total);
		
		JOptionPane.showMessageDialog(null, msg);
	}
}
