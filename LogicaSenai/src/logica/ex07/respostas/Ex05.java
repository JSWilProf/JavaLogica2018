package logica.ex07.respostas;

import logica.lib.Util;

public class Ex05 {
	public static void main(String[] args) {
		int num = Util.leInteiro("Informe o nº para o cálculo do Fatorial");
		
		while(num > 0) {
			Util.escreval("O valor calculado com a função fatorial para ", num, " é: " , fatorial(num));
			
			num = Util.leInteiro("Informe o nº para o cálculo do Fatorial");
		}
	}
	
	public static int fatorial(int num) {
		if(num > 1) return num * fatorial(num - 1);
		return 1;
	}
}
