package logica.ex07.respostas;

import logica.lib.Util;

public class Ex01 {
	public static void main(String[] args) {
		int num = Util.leInteiro("Informe um nº");

		while (num > 0) {
			Util.escreval("O nº ", num, " é ", ePar(num) ? "Par" : "Impar");

			num = Util.leInteiro("Informe um nº");
		}
	}

	public static boolean ePar(int num) {
		return num % 2 == 0;
	}
}