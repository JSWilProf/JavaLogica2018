package logica.ex07.respostas;

import logica.lib.Util;

public class Ex04 {
	public static void main(String[] args) {
		int num = Util.leInteiro("Informe um nº");
		
		while(num > 0) {
			tabuada(num);
			
			num = Util.leInteiro("Informe um nº");
		}
	}
	
	public static void tabuada(int num) {
		String msg = "";
		for (int i = 1; i <= 10; i++) {
			msg += Util.formata(num, " x ", i, " = ", num * i, "\n");
		}
		Util.escreval(msg, "\n");
	}
}
