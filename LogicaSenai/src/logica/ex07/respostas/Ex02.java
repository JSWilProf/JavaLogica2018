package logica.ex07.respostas;

import logica.lib.Util;

public class Ex02 {
	public static void main(String[] args) {
		int num = Util.leInteiro("Informe um nº");
		
		while(num > 0) {
			Util.escreval("O inverso do nº ", num, " é " , inverte(num));
			
			num = Util.leInteiro("Informe um nº");
		}
	}
	
	public static int inverte(int num) {
		int inv = 0;
		
		while(num > 0) {
			inv = inv * 10 + num % 10;
			num /= 10;
		}
		
		return inv;
	}
}
