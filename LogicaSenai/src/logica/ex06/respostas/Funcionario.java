package logica.ex06.respostas;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//Plain Old Java Object - POJO
@Data
@AllArgsConstructor
@NoArgsConstructor
class Funcionario implements Comparable<Funcionario> {
	private String nome;
	private double salario;

	@Override
	public String toString() {
		return String.format("%-16s %,11.2f", nome, salario);
	}

	@Override
	public int compareTo(Funcionario outro) {
		return -(int)Math.ceil(salario - outro.salario);
	}
}
