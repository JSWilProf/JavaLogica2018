package logica.ex06.respostas;

import logica.lib.Util;

public class Ex04 {
	public static void main(String[] args) {
		String[] nomes = new String[5];
		double[] sals = new double[5];
		int idx = 0;
		
		// Cataloga os nomes e salarios
		String nome = Util.leTexto("Informe o nome");
		
		while(!nome.equalsIgnoreCase("fim")) {
			double salario = Util.leReal("Informe o Salário");
			
			// expansão dos vetores
			if(idx >= nomes.length) {
				//Cria novos vetores maiores que os atuais
				String[] novoNomes = new String[nomes.length + 5];
				double[] novoSals = new double[sals.length + 5];
				
				// copiar os dados dos vetores antigos para os novos
				for (int i = 0; i < nomes.length; i++) {
					novoNomes[i] = nomes[i];
					novoSals[i] = sals[i];
				}
				
				// Passa a utilizar os novos vetores
				nomes = novoNomes;
				sals = novoSals;
			}
			
			nomes[idx] = nome;
			sals[idx++] = salario;
 			
			nome = Util.leTexto("Informe o nome");
		}
		
		// Ordena os vetores
		boolean trocou;
		do {
			trocou = false;
			for (int i = 0; i < idx - 1; i++) {
				if (sals[i] < sals[i + 1]) {
					String  tempNome = nomes[i];
					nomes[i] = nomes[i + 1];
					nomes[i + 1] = tempNome;
					
					double tempSal = sals[i];
					sals[i] = sals[i + 1];
					sals[i + 1] = tempSal;
					
					trocou = true;
				}
			}
		} while(trocou);
		
		// Lista o relatório
		String msg = "Funcionário\t\t\t\tSalário\n"
				   + "-----------\t\t\t-------\n";
		for (int i = 0; i < idx; i++) {
			msg += String.format("%-16s %,11.2f\n", nomes[i], sals[i]);
		}
		Util.escreval(msg);
	}
}
