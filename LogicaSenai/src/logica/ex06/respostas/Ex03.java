package logica.ex06.respostas;

import static logica.lib.Util.*;

import java.util.Arrays;

public class Ex03 {
	public static void main(String[] args) {
		int[] lista = new int[10];
		
		for (int i = 0; i < lista.length; i++) {
			lista[i] = leInteiro("Informe o " + (i + 1) + "º nº");
		}
		
		Arrays.sort(lista);
		
		
		String msg = "Números pares em ordem crescente\n";
		for (int i = 0; i < lista.length; i++) {
			if(lista[i] % 2 == 0) {
				msg += lista[i] + " ";
			}
		}
		
		msg += "\n\nNúmeros pares em ordem decrescente\n";
		for (int i = lista.length -1; i >= 0 ; i--) {
			if(lista[i] % 2 != 0) {
				msg += lista[i] + " ";
			}
		}
		
		escreval(msg);
	}
}
