package logica.ex06.respostas;

import logica.lib.Util;

public class Ex02 {
	public static void main(String[] args) {
		String[] lista = new String[10];
		
		for (int i = 0; i < lista.length; i++) {
			lista[i] = Util.leTexto("Informe o "+(i+1)+"º Nome");
		}
		
		String nome = Util.leTexto("Informe o nome a ser pesquisado");
		while(!nome.equalsIgnoreCase("fim")) {
			boolean achou = false;
			
			for (int i = 0; i < lista.length; i++) {
				if(lista[i].equalsIgnoreCase(nome)) {
					achou = true;
					break;
				}
			}
			
			if(achou) {
				Util.escreval("O nome " + nome + " foi encontrato");
			} else {
				Util.escreval("O nome não " + nome + " foi encontrato");
			}
			
			nome = Util.leTexto("Informe o nome a ser pesquisado");
		}
	}
}
