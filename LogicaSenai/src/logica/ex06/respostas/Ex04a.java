package logica.ex06.respostas;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import logica.lib.Util;

public class Ex04a {
	public static void main(String[] args) {
		List<Funcionario> funcionarios = new ArrayList<>();

		// Cataloga os nomes e salarios
		String nome = Util.leTexto("Informe o nome");

		while (!nome.equalsIgnoreCase("fim")) {
			double salario = Util.leReal("Informe o Salário");

			funcionarios.add(new Funcionario(nome, salario));
			
			nome = Util.leTexto("Informe o nome");
		}

		 // Ordena os vetores
		

		// Lista o relatório
		String msg = "Funcionário\t\t\t\tSalário\n" + "---------\t\t\t\t-------\n";
			Util.escreval(msg +
					funcionarios.stream()
						.sorted(Comparator.comparing(Funcionario::getSalario)
								.reversed())
						.map(Funcionario::toString)
						.collect(Collectors.joining("\n"))
				);
	}
}
