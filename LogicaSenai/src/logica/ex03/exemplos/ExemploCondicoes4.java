package logica.ex03.exemplos;

import javax.swing.JOptionPane;



public class ExemploCondicoes4 {
	public static void main(String[] args) {
		String temp = JOptionPane.showInputDialog("Informe o mês");
		int mes = Integer.parseInt(temp);

		String msg = "";

		switch (mes) {
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
			msg = "1º Semestre";
			break;
		case 7:
		case 8:
		case 9:
		case 10:
		case 11:
		case 12:
			msg = "2º semestre";
			break;
		default:
			msg = "Mês inválido";
			break;
		}
	
		
		JOptionPane.showMessageDialog(null, msg);
	}
}
