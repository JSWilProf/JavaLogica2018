package logica.ex03.respostas;

import javax.swing.JOptionPane;

public class Ex01 {
	public static void main(String[] args) {
		String temp = JOptionPane.showInputDialog("Informe o 1º nº");
		int num1 = Integer.parseInt(temp);
		
		temp = JOptionPane.showInputDialog("Informe o 2º nº");
		int num2 = Integer.parseInt(temp);
		
		
		String resp;
		if(num1 != num2) {
			resp = "São Diferentes";
		} else {
			resp = "São Iguais";
		}
		
		JOptionPane.showMessageDialog(null, resp);
	}
}
