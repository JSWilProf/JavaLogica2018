package logica.ex03.respostas;

import javax.swing.JOptionPane;

import logica.lib.Util;

public class Ex02b {
	public static void main(String[] args) {
		JOptionPane.showMessageDialog(null, 
				 Util.leInteiro("Informe um  nº") % 2 == 0 ? "É par" : "É impar");
	}
}
