package logica.ex03.respostas;

import javax.swing.JOptionPane;

import logica.lib.Util;

public class Ex05a {
	public static void main(String[] args) {
		int num1 = Util.leInteiro("Informe o 1º nº");
		int num2 = Util.leInteiro("Informe o 2º nº");

		if(num1 > num2) {
			int temp = num1;
			num1 = num2;
			num2 = temp;
		} 
		
		JOptionPane.showMessageDialog(null, num1 + " " + num2);
	}
}
