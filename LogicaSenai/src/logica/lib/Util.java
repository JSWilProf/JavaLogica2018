package logica.lib;

import java.text.NumberFormat;
import java.text.ParsePosition;

import javax.swing.JOptionPane;

public class Util {
	private static NumberFormat fmt = NumberFormat.getNumberInstance();
		
	public static int leInteiro(String msg) {
		String temp = JOptionPane.showInputDialog(msg);
		return Integer.parseInt(temp);
	}
	
	public static double leReal(String msg) {
		String temp = JOptionPane.showInputDialog(msg);
		return fmt.parse(temp, new ParsePosition(0)).doubleValue();
	}
	
	public static String leTexto(String msg) {
		return JOptionPane.showInputDialog(msg);
	}

	public static String formata(Object ... args) {
		String txt = "";
		for (int i = 0; i < args.length; i++) {
			Object arg = args[i];
			
			if (arg instanceof String) {
				txt += String.format("%s", arg);
			} else if (arg instanceof Integer) {
				txt += String.format("%d", arg);
			} else if (arg instanceof Double) {
				txt += String.format("%,.2f", arg);
			}
		}
		return txt;
	}
		
	public static void escreval(Object ... args) {
		JOptionPane.showMessageDialog(null, formata(args));
	}

	public static void ordenar(int[] num) {
		boolean trocou;
		do {
			trocou = false;
			for (int i = 0; i < num.length - 1; i++) {
				if (num[i] > num[i + 1]) {
					int temp = num[i];
					num[i] = num[i + 1];
					num[i + 1] = temp;
					trocou = true;
				}
			}
		} while(trocou);
	}
}
