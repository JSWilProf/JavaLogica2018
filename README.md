# Repositório da Turma Java Lógica 2018 Jul - Senai Informatica 1.32
## Bem Vindo
Aqui são disponibilizados os projetos Java Web, as apresentações, exercícios e respostas.
## Como obter
Para obter uma cópia deste conteúdo basta utilizar o comando:

```
git clone git@gitlab.com:JSWilProf/JavaLogica2018.git
```

Também é possível fazer o download através do link
[Java Lógica 2018](https://gitlab.com/JSWilProf/JavaLogica2018)

# Ementa

## Lógica de Programação (40h)

- Características da Linguagem
- Ferramentas de programação
- Instalação do Kit de desenvolvimento
- Tipos de dados da Linguagem
- Variáveis e constantes
- Operadores Lógicos e relacionais
- Conversão entre tipos
- Instruções condicionais e seleções Múltiplas
- Laços
- Coleção de Dados
- Tratamento de Erros
- Sub-rotinas e recursividade
